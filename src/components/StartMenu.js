import React, { PureComponent } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { startProgram, closeMenu } from "../actions";

const Menu = styled.div`
  overflow: hidden;
  background-color: #c0c0c0;
  position: absolute;
  height: 400px;
  width: ${props => (props.open ? "220px" : "0px")};
  top: ${props => (props.top ? `${props.top - 152}px` : "0px")};
  left: 50px;
  transition: 0.2s all;
  display: flex;
`;

const Gradient = styled.div`
  background: linear-gradient(
    to bottom,
    #000094 0%,
    #0000ff 82%,
    #0000ff 88%,
    #000094 100%
  );
  writing-mode: vertical-lr;
  text-orientation: sideways-right;
  color: white;
  font-size: 35px;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
  padding-bottom: 30px;
`;

const RotatedText = styled.div``;

const MenuList = styled.div``;

const MenuImg = styled.img`
  margin-right: 8px;
`;

const MenuItem = styled.a`
  text-decoration: none;
  color: black;
  display: flex;
  padding: 10px;
  font-size: 16px;
  align-items: center;
`;

class StartMenu extends PureComponent {
  render() {
    const { menu, startProgram } = this.props;
    return (
      <Menu open={menu.open} top={menu.top}>
        <Gradient>
          <RotatedText>
            <strong>William</strong>95
          </RotatedText>
        </Gradient>
        <MenuList>
          <MenuItem
            href="#paint"
            onClick={e => {
              e.preventDefault();
              startProgram("paint");
            }}
          >
            <MenuImg
              width="32px"
              src={require("../applications/Paint/icon.png")}
            />
            BoeverPaint
          </MenuItem>
        </MenuList>
      </Menu>
    );
  }
}

const mapStateToProps = ({ menu }) => {
  return { menu };
};

const mapDispatchToProps = dispatch => {
  return {
    startProgram: name => {
      dispatch(startProgram(name));
      dispatch(closeMenu());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartMenu);
