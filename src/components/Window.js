import React, { PureComponent } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { ItemTypes } from "../dnd/constants";
import { DragSource } from "react-dnd";
import { endProgram } from "../actions";

const windowSource = {
  beginDrag(props) {
    return {
      name: props.name
    };
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const WindowDiv = styled.div`
  border: 1px solid black;
  position: absolute;
  min-width: ${props => (props.program.width ? props.program.width : 300)}px;
  max-width: ${props =>
    props.program.width ? props.program.width + 40 : 300 + 40}px;
  min-height: ${props =>
    props.program.width ? props.program.width + 80 : 300}px;
  top: ${props => (props.program.y ? props.program.y : 0)}px;
  left: ${props => (props.program.x ? props.program.x : 0)}px;
  display: flex;
  flex-direction: column;
  padding: 1px;
  background-color: #bdbdbd;
  border-top: 1px solid #fafafa;
  border-left: 1px solid #fafafa;

  outline: 1px solid #e1e1e1;
`;

const TitleBar = styled.div`
  background: linear-gradient(to right, #00007b 0%, #087eca 100%);
  color: white;
  padding: 3px;
  display: flex;
  justify-content: space-between;
`;

const Icon = styled.img`
  width: 16px;
  margin-right: 4px;
`;

const CloseButton = styled.button`
  background-color: #c1c1c1;
  color: black;
`;

const Content = styled.div`
  background-color: #bdbdbd;
  flex-grow: 1;
`;

class Window extends PureComponent {
  render() {
    const {
      title,
      name,
      children,
      endProgram,
      program,
      icon,
      connectDragSource
    } = this.props;

    return (
      <WindowDiv program={program}>
        {connectDragSource(
          <div>
            <TitleBar>
              <div>
                <Icon src={icon} alt={title} />

                {title}
              </div>
              <CloseButton
                href="#close"
                onClick={e => {
                  e.preventDefault();
                  endProgram(name);
                }}
              >
                X
              </CloseButton>
            </TitleBar>
          </div>
        )}
        <Content>{children}</Content>
      </WindowDiv>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    endProgram: name => dispatch(endProgram(name))
  };
};

const DraggableWindow = DragSource(ItemTypes.WINDOW, windowSource, collect)(
  Window
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DraggableWindow);
