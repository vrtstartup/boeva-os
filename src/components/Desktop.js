import React, { PureComponent } from "react";
import { ItemTypes } from "../dnd/constants";
import { DropTarget } from "react-dnd";
import { connect } from "react-redux";
import { moveWindow } from "../actions";

import styled from "styled-components";

const DesktopDiv = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const desktopTarget = {
  drop(props, monitor) {
    const { moveWindow } = props;
    const { x, y } = monitor.getClientOffset();
    const item = monitor.getItem();

    moveWindow(item.name, x, y);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class Desktop extends PureComponent {
  render() {
    const { connectDropTarget } = this.props;
    return connectDropTarget(
      <div>
        <DesktopDiv>{this.props.children}</DesktopDiv>
      </div>
    );
  }
}

const DroppableDesktop = DropTarget(ItemTypes.WINDOW, desktopTarget, collect)(
  Desktop
);

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return {
    moveWindow: (name, x, y) => {
      dispatch(moveWindow(name, x, y));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DroppableDesktop);
