import React, { PureComponent } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { toggleMenu } from "../actions";

const Button = styled.button`
  vertical-align: middle;
  font-family: Courrier;
  font-size: 22px;
  background: #c0c0c0;
  border-top: 1px solid #fff;
  border-left: 1px solid #fff;
  border-right: 1px solid #000;
  border-bottom: 1px solid #000;
  line-height: 14px;
  padding: 2px;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
  background: ${props =>
    props.open
      ? "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACAQMAAABIeJ9nAAAABlBMVEW9vb3///8EwsWUAAAADElEQVQI12NoYHAAAAHEAMFJRSpJAAAAAElFTkSuQmCC) repeat"
      : null};
`;

const Img = styled.img`
  height: 50px;
`;

class StartButton extends PureComponent {
  render() {
    const { toggleMenu, menu } = this.props;
    return (
      <Button onClick={toggleMenu} open={menu.open}>
        <Img src={require("../assets/wb.png")} alt="William" />
      </Button>
    );
  }
}

const mapStateToProps = ({ menu }) => {
  return {
    menu
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleMenu: e => {
      e.preventDefault();
      dispatch(toggleMenu(e.clientY));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartButton);
