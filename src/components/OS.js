import React, { PureComponent } from "react";
import { connect } from "react-redux";
import TaskBar from "./TaskBar";
import Desktop from "./Desktop";
import Loadable from "react-loadable";

const Loading = () => <p>loading</p>;

const Paint = Loadable({
  loader: () => import("../applications/Paint"),
  loading: Loading
});

class OS extends PureComponent {
  render() {
    const { programs } = this.props;
    console.log(programs);
    return (
      <React.Fragment>
        <Desktop>
          {Object.keys(programs || {}).map(name => {
            const program = programs[name];
            if (program.open) {
              switch (name) {
                case "paint":
                  return <Paint key={name} program={program} />;
                default:
                  return null;
              }
            } else {
              return null;
            }
          })}
        </Desktop>
        <TaskBar />
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ task_manager }) => {
  return {
    programs: task_manager.programs
  };
};

export default connect(mapStateToProps)(OS);
