import React, { PureComponent } from "react";
import styled from "styled-components";

import StartButton from "./StartButton";
import StartMenu from "./StartMenu";

const Bar = styled.div`
  position: absolute;
  bottom: 0px;
  left: 0px;
  top: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 4px;
`;

class TaskBar extends PureComponent {
  render() {
    return (
      <React.Fragment>
        <StartMenu />

        <Bar>
          <StartButton />
        </Bar>
      </React.Fragment>
    );
  }
}

export default TaskBar;
