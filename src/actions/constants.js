export const OPEN_MENU = "OPEN_MENU";
export const CLOSE_MENU = "CLOSE_MENU";
export const TOGGLE_MENU = "TOGGLE_MENU";
export const START_PROGRAM = "START_PROGRAM";
export const END_PROGRAM = "END_PROGRAM";
export const MOVE_WINDOW = "MOVE_WINDOW";
export const RESET = "RESET";
