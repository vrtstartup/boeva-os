import {
  OPEN_MENU,
  CLOSE_MENU,
  TOGGLE_MENU,
  START_PROGRAM,
  END_PROGRAM,
  MOVE_WINDOW
} from "./constants";

/* menu */
const openMenu = top => {
  return {
    type: OPEN_MENU,
    top
  };
};
const closeMenu = () => {
  return {
    type: CLOSE_MENU
  };
};
const toggleMenu = top => {
  return {
    type: TOGGLE_MENU,
    top
  };
};

/* task_manager */
const startProgram = name => {
  return {
    type: START_PROGRAM,
    name
  };
};

const endProgram = name => {
  return {
    type: END_PROGRAM,
    name
  };
};

const moveWindow = (name, x, y) => {
  return {
    type: MOVE_WINDOW,
    name,
    x,
    y
  };
};

const reset = () => {
  return {
    type: "RESET"
  };
};

export {
  openMenu,
  closeMenu,
  toggleMenu,
  startProgram,
  endProgram,
  moveWindow,
  reset
};
