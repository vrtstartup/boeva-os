import React, { Component } from "react";
import { DragDropContextProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

import createStore from "./store";

import OS from "./components/OS";
import Paint from "./applications/Paint";

const { store, persistor } = createStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <PersistGate loading={null} persistor={persistor}>
            <DragDropContextProvider backend={HTML5Backend}>
              <Switch>
                <Route path="/" exact component={OS} />
                <Route
                  path="/paint"
                  exact
                  render={() => (
                    <div
                      style={{
                        transform: "scale(2)",
                        transformOrigin: "top left",
                        width: "640px",
                        height: "400px"
                      }}
                    >
                      <Paint program={{ width: 320, height: 200 }} />
                    </div>
                  )}
                />
              </Switch>
            </DragDropContextProvider>
          </PersistGate>
        </Router>
      </Provider>
    );
  }
}

export default App;
