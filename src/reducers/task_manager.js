import {
  START_PROGRAM,
  END_PROGRAM,
  MOVE_WINDOW,
  RESET
} from "../actions/constants";

const task_manager = (state = { programs: {} }, action) => {
  const programs = Object.assign({}, null, state.programs);
  const actionDefaults = {
    open: true,
    width: 320,
    height: 200,
    x: 100,
    y: 100
  };

  switch (action.type) {
    case START_PROGRAM:
      programs[action.name] = programs[action.name]
        ? programs[action.name]
        : actionDefaults;
      programs[action.name].open = true;
      return { ...state, programs };
    case END_PROGRAM:
      programs[action.name] = programs[action.name]
        ? programs[action.name]
        : actionDefaults;
      programs[action.name].open = false;
      return { ...state, programs };
    case MOVE_WINDOW:
      programs[action.name] = programs[action.name]
        ? programs[action.name]
        : actionDefaults;
      programs[action.name].x = action.x;
      programs[action.name].y = action.y;
      return { ...state, programs };
    case RESET:
      return { programs: {} };
    default:
      return state;
  }
};

export default task_manager;
