import { combineReducers } from "redux";
import menu from "./menu";
import task_manager from "./task_manager";

const reducers = combineReducers({ menu, task_manager });

export default reducers;
