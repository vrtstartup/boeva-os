import {
  OPEN_MENU,
  CLOSE_MENU,
  TOGGLE_MENU,
  RESET
} from "../actions/constants";

const menu = (state = { open: false, top: 0 }, action) => {
  switch (action.type) {
    case OPEN_MENU:
      return { ...state, open: true, top: action.top };
    case CLOSE_MENU:
      return { ...state, open: false };
    case TOGGLE_MENU:
      return { ...state, open: !state.open, top: action.top };
    case RESET:
      return { open: false };
    default:
      return state;
  }
};

export default menu;
