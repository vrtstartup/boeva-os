import React, { PureComponent } from "react";
import styled from "styled-components";

const ColorSwatch = styled.div`
  background-color: ${props => props.color};
  width: 16px;
  height: 16px;
  border-top: 1px solid black;
  border-left: 1px solid black;
  border-right: 1px solid #7b7c7b;
  border-bottom: 1px solid #7b7c7b;
  margin-right: 2px;
  margin-bottom: 2px;
  cursor: pointer;
`;

const SelectedColor = styled.div`
  min-width: 20px;
  min-height: 20px;
  max-height: 20px;
  border: 1px inset black;
  background-color: ${props => props.color};
  margin-right: 4px;
  flex-grow: 1;
`;

const ColorSwatches = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const ColorPickerDiv = styled.div`
  display: flex;
  padding: 4px;
  align-items: center;
  margin-top: 4px;
`;

class Colorpicker extends PureComponent {
  render() {
    const colors = [
      "#000000",
      "#808080",
      "#75140C",
      "#808026",
      "#377E22",
      "#367E7F",
      "#00017B",
      "#75157C",
      "#808049",
      "#173F3F",
      "#357EF7",
      "#173F7C",
      "#7517F5",
      "#784315",
      "#FFFFFF",
      "#C0C0C0",
      "#EB3323",
      "#FFFF54",
      "#74FB4C",
      "#73FBFD",
      "#0004F5",
      "#EA34F7",
      "#FFFF92",
      "#74FB8E",
      "#A0FCFE",
      "#8080F7",
      "#EB337F",
      "#EF8750"
    ];

    const { selectedColor, setColor } = this.props;

    return (
      <ColorPickerDiv>
        <SelectedColor color={selectedColor} />
        <ColorSwatches>
          {colors.map(color => (
            <ColorSwatch
              onClick={() => setColor(color)}
              key={`color-${color}`}
              color={color}
            />
          ))}
        </ColorSwatches>
      </ColorPickerDiv>
    );
  }
}

export default Colorpicker;
