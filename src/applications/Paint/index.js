import React, { PureComponent } from "react";
import styled from "styled-components";

import Window from "../../components/Window";

import Canvas from "./Canvas";
import Tools from "./Tools";
import Colorpicker from "./Colorpicker";

const App = styled.div`
  position: relative;
`;

const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

class Paint extends PureComponent {
  state = { selectedColor: "#000000", brushSize: 1 };

  render() {
    const { program } = this.props;
    const { selectedColor, brushSize } = this.state;

    return (
      <Window
        title="BoeverPaint"
        name="paint"
        program={program}
        icon={require("./icon.png")}
      >
        <App>
          <Canvas color={selectedColor} brushSize={brushSize} />
          <Bottom>
            <Tools
              setTool={px => this.setState({ brushSize: px })}
              selectedTool={brushSize}
              color={selectedColor}
            />
            <Colorpicker
              selectedColor={selectedColor}
              setColor={color => this.setState({ selectedColor: color })}
            />
          </Bottom>
        </App>
      </Window>
    );
  }
}

export default Paint;
