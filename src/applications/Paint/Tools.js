import React, { PureComponent } from "react";
import styled from "styled-components";

const SIZES = [1, 2, 4, 8, 12];

const ToolDiv = styled.div`
  margin-left: 4px;
  margin-right: 4px;
  min-width: 24px;
`;

const Hit = styled.div`
  padding: 2px;
  cursor: pointer;
`;

const Line = styled.div`
  background-color: ${props => props.color};
  min-width: 16px;
  height: ${props => props.size}px;
  margin-bottom: 2px;
  border: ${props =>
    props.isSelected
      ? props.color !== "#FFFFFF"
        ? "1px solid white"
        : "1px solid black"
      : "none"};
`;

class Tools extends PureComponent {
  render() {
    const { color, setTool, selectedTool } = this.props;

    return (
      <ToolDiv>
        {SIZES.map(size => (
          <Hit key={size} onClick={e => setTool(size)}>
            <Line
              size={size}
              color={color}
              isSelected={selectedTool === size}
            />
          </Hit>
        ))}
      </ToolDiv>
    );
  }
}

export default Tools;
