import React, { PureComponent } from "react";
import io from "socket.io-client";
import uuidv1 from "uuid/v1";
import styled from "styled-components";

class Canvas extends PureComponent {
  canvas = null;
  ctx = null;
  started = false;
  uuid = null;
  socket = null;
  buffer = [];
  toDrawBuffer = [];

  componentDidMount() {
    this.ctx = this.canvas.getContext("2d");
    this.ctx.lineCap = "round";
    this.canvas.addEventListener(
      "mousedown",
      e => {
        this.ev_canvas(e);
      },
      false
    );
    this.canvas.addEventListener("mousemove", e => this.ev_canvas(e), false);
    this.canvas.addEventListener(
      "mouseup",
      e => {
        this.ev_canvas(e);
      },
      false
    );

    // socket
    this.socket = io("https://boever-server.herokuapp.com/");
    //this.socket = io("http://localhost:4000");
    this.socket.on("initial", initialBuffer => {
      initialBuffer.forEach(b => this.toDrawBuffer.push(b));
    });

    this.socket.on("draw", ({ color, buffer, size }) => {
      this.toDrawBuffer.push({ color, buffer, size });
    });

    window.requestAnimationFrame(() => this.processBuffers());
  }

  processBuffers() {
    if (this.toDrawBuffer.length && !this.started) {
      const item = this.toDrawBuffer.shift();
      const { color, buffer, size } = item;
      if (buffer.length) {
        const first = buffer.shift();
        this.ctx.strokeStyle = color;
        this.ctx.lineWidth = size;

        this.ctx.beginPath();
        this.ctx.moveTo(first.x, first.y);
        buffer.forEach(({ x, y }) => {
          console.log(`Draw ${x}, ${y}`);

          this.ctx.lineTo(x, y);
          this.ctx.stroke();
        });
      }
    }
    window.requestAnimationFrame(() => this.processBuffers());
  }

  componentWillUnmout() {
    this.canvas.removeEventListener("mousedown");
    this.canvas.removeEventListener("mousemove");
    this.canvas.removeEventListener("mouseup");
  }

  ev_canvas(ev) {
    if (ev.layerX || ev.layerX == 0) {
      ev._x = ev.layerX;
      ev._y = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) {
      ev._x = ev.offsetX;
      ev._y = ev.offsetY;
    }
    if (!ev._type) ev._type = ev.type;

    if (ev.type === "mousedown") this.mousedown(ev);
    if (ev.type === "mousemove") this.mousemove(ev);
    if (ev.type === "mouseup") this.mouseup(ev);
  }

  mousedown(ev) {
    this.started = true;

    this.ctx.strokeStyle = this.props.color;
    this.ctx.lineWidth = this.props.brushSize;

    this.ctx.beginPath();
    this.ctx.moveTo(ev._x, ev._y);
    this.buffer.push({ x: ev._x, y: ev._y });
  }

  mousemove(ev) {
    if (this.started) {
      this.buffer.push({ x: ev._x, y: ev._y });
      this.ctx.lineTo(ev._x, ev._y);
      this.ctx.stroke();
    }
  }

  mouseup(ev) {
    if (this.started) {
      this.mousemove(ev);

      if (this.buffer.length) {
        this.flushBuffer();
      }
      this.started = false;
    }
  }

  flushBuffer() {
    if (this.buffer.length) {
      this.socket.emit("draw", {
        color: this.props.color,
        size: this.props.brushSize,
        buffer: this.buffer
      });
    }

    this.buffer = [];
  }

  render() {
    const { program } = this.props;
    return (
      <canvas
        ref={ref => (this.canvas = ref)}
        style={{ backgroundColor: "white" }}
        width="360"
        height="320"
      />
    );
  }
}

export default Canvas;
